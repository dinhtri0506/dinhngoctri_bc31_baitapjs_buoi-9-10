function return_month_day_year_output() {
  var dateString = document.getElementById("datepicker").value;
  var parts = dateString.split("/");
  var day = parts[1] * 1;
  var month = parts[0] * 1;
  var year = parts[2] * 1;
  function return_month_or_day_output(monthOrDay) {
    if (monthOrDay < 10) {
      return "0" + monthOrDay;
    }
    return monthOrDay;
  }
  var monthOutput = return_month_or_day_output(month);
  var dayOutput = return_month_or_day_output(day);
  var output = `${monthOutput}/${dayOutput}/${year}`;
  if (!isNaN(day) && !isNaN(month) && !isNaN(year)) {
    document.getElementById("datepicker").value = output;
  }
}

function get_employee_information() {
  return_month_day_year_output();
  var account = document.getElementById("tknv").value;
  var fullName = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var date = document.getElementById("datepicker").value;
  var basicSalary = document.getElementById("luongCB").value * 1;
  var position = document.getElementById("chucvu").value;
  var workingTime = document.getElementById("gioLam").value * 1;
  var employee = new Employee(
    account,
    fullName,
    email,
    password,
    date,
    basicSalary,
    position,
    workingTime
  );
  return employee;
}
