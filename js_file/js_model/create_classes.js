function Employee(
  _account,
  _fullName,
  _email,
  _password,
  _date,
  _basicSalary,
  _position,
  _workingTime
) {
  this.account = _account;
  this.fullName = _fullName;
  this.email = _email;
  this.password = _password;
  this.date = _date;
  this.basicSalary = _basicSalary;
  this.position = _position;
  this.workingTime = _workingTime;
  this.calculate_total_salary = function () {
    var totalSalary = 0;
    if (this.position == "Sếp") {
      totalSalary = this.basicSalary * 3;
    }
    if (this.position == "Trưởng phòng") {
      totalSalary = this.basicSalary * 2;
    }
    if (this.position == "Nhân viên") {
      totalSalary = this.basicSalary * 1;
    }
    return new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(totalSalary);
  };
  this.classification = function () {
    if (this.workingTime >= 192) {
      return "Nhân viên xuất sắc";
    }
    if (this.workingTime >= 176) {
      return "Nhân viên giỏi";
    }
    if (this.workingTime >= 160) {
      return "Nhân viên khá";
    }
    return "Nhân viên trung bình";
  };
}

var employeeList = [];
