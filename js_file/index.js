function add_employee_information() {
  var employeeInfo = get_employee_information();
  var validAccount = validate_employee_account(
    false,
    true,
    "Tài khoản nhân viên trùng với tài khoản sẵn có!",
    "",
    "inline-block",
    "none"
  );
  var validForm = validate_employee_info();
  if (validAccount && validForm) {
    console.log("success");
    employeeList.push(employeeInfo);
    render_employee_information();
    upload_info_to_localStorage();
    alert("Thêm nhân viên thành công");
  } else {
    console.log("fail");
  }
}

function delete_employee_info(account) {
  var index = find_index(account);
  employeeList.splice(index, 1);
  render_employee_information();
  upload_info_to_localStorage();
}

var accountEl = document.getElementById("tknv");
function change_employee_info(account) {
  reset_form();
  var index = find_index(account);
  pull_employee_info_back_to_input_element(employeeList, index);
  accountEl.disabled = true;
}

function update_employee_info() {
  var employeeAccount = accountEl.value;
  var index = find_index(employeeAccount);
  var employeeInfo = get_employee_information();
  var validAccount = validate_employee_account(
    true,
    false,
    "",
    "Tài khoản hiện chưa có sẵn để cập nhật!",
    "none",
    "inline-block"
  );
  var validForm = validate_employee_info();
  if (validAccount && validForm) {
    employeeList.splice(index, 1, employeeInfo);
    render_employee_information();
    upload_info_to_localStorage();
    alert("Cập nhật thành công");
  }
}

document
  .getElementById("btnTimNV")
  .addEventListener("click", search_employee_by_their_classification);
function search_employee_by_their_classification() {
  var renderArray = create_classification_list();
  render_classification_list(renderArray[0], renderArray[1]);
}
