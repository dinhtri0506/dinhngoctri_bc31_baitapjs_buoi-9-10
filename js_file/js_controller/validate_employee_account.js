function validate_employee_account(
  is_01,
  is_02,
  notice_01,
  notice_02,
  display_01,
  display_02
) {
  var employeeAccount = accountEl.value;
  var index = find_index(employeeAccount);
  var noticeEl = document.getElementById("tbTKNV");
  if (employeeAccount != "") {
    if (employeeAccount.match(/^[0-9]{4,6}$/)) {
      if (index > -1) {
        noticeEl.style.display = display_01;
        noticeEl.innerText = notice_01;
        return is_01;
      } else {
        noticeEl.style.display = display_02;
        noticeEl.innerText = notice_02;
        return is_02;
      }
    } else {
      noticeEl.style.display = "block";
      noticeEl.innerText = "Tài khoản nhân viên gồm 4-6 ký số";
      return false;
    }
  } else {
    noticeEl.style.display = "block";
    noticeEl.innerText = "Vui lòng nhập tài khoản nhân viên!";
    return false;
  }
}
