function validate(key, validKey, noticeEl, noticeContent_01, noticeContent_02) {
  var contentValidated = key;
  var noticeEl = document.getElementById(noticeEl);
  if (contentValidated != "") {
    if (contentValidated.match(validKey)) {
      noticeEl.style.display = "none";
      noticeEl.innerHTML = "";
      return true;
    } else {
      noticeEl.style.display = "inline-block";
      noticeEl.innerHTML = noticeContent_02;
      return false;
    }
  } else {
    noticeEl.style.display = "inline-block";
    noticeEl.innerHTML = noticeContent_01;
    return false;
  }
}

function removeAscent(str) {
  if (str === null || str === undefined) return str;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  return str;
}

function check_max_day_of_month(maxDay, month, day, noticeEl) {
  if (day > 0 && day <= maxDay) {
    noticeEl.style.display = "none";
    noticeEl.innerHTML = "";
    return true;
  }
  noticeEl.style.display = "inline-block";
  noticeEl.innerHTML = `<span class="text-secondary">mm/<b class="text-danger">dd</b>/yyyy: Tháng ${month} có tối đa ${maxDay} ngày</span>`;
  return false;
}

function validate_month_day_value(maxDayOfFeb, month, day, noticeEl) {
  if (month > 0 && month <= 12) {
    if ((month < 8 && month % 2 != 0) || (month > 7 && month % 2 == 0)) {
      var validMonthHave31Days = check_max_day_of_month(
        31,
        month,
        day,
        noticeEl
      );
      return validMonthHave31Days;
    }
    if (month != 2) {
      var validMonthHave30Days = check_max_day_of_month(
        30,
        month,
        day,
        noticeEl
      );
      return validMonthHave30Days;
    }
    var validFeb = check_max_day_of_month(maxDayOfFeb, month, day, noticeEl);
    return validFeb;
  }
  noticeEl.style.display = "inline-block";
  noticeEl.innerHTML = `<span class="text-secondary"><b class="text-danger">mm</b>/dd/yyyy: Giá trị của tháng phải nằm trong khoảng từ 1 đến 12</span>`;
}

function validate_employee_info() {
  var employee = get_employee_information();
  var name = function () {
    var validName = /^([a-zA-Z]+[\s]?)+$/;
    var nameAfterRemoveAscent = removeAscent(employee.fullName);
    var validatedName = validate(
      nameAfterRemoveAscent,
      validName,
      "tbTen",
      "Vui lòng nhập họ và tên!",
      "Họ và tên chỉ bao gồm chữ cái và khoảng trắng (ký tự đầu tiên phải là chữ cái)"
    );
    return validatedName;
  };
  var email = function () {
    var validEmail =
      /^[a-zA-Z]([\w\.\_\-]){3,11}@([a-zA-Z]{2,6})\.([a-zA-Z]{2,3})$/;
    var emailNotice = `Email không hợp lệ <br />
      Email hợp lệ có dạng: tên@mail.com <br />
      <ul class="list-style-none text-secondary">
      <li><b>tên:</b> có từ 4 đến 12 ký tự (bao gồm chữ cái, số, dấu chấm, dấu gạch nối và dấu gạch chân) với ký tự đầu tiên là chữ cái</li>
      <li><b>mail:</b> có từ 2 đến 6 ký tự (chỉ bao gồm chữ cái)</li>
      <li><b>com:</b> có từ 2 đến 3 ký tự (chỉ bao gồm chữ cái)</li>
      <li><b>ví dụ: alice_19@gmail.com</b></li>
      </ul>`;
    var validatedEmail = validate(
      employee.email,
      validEmail,
      "tbEmail",
      "Vui lòng nhập email!",
      emailNotice
    );
    return validatedEmail;
  };
  var password = function () {
    var validPassword = /^(?=.*\d)(?=.*[A-Z])(?=.*\W)\S{8,}$/;
    var validatedPassword = validate(
      employee.password,
      validPassword,
      "tbMatKhau",
      "Vui lòng nhập mật khẩu!",
      "Mật khẩu cần có ít nhất 8 ký tự (trong đó có ít nhất 1 chữ hoa, 1 ký số và 1 ký tự đặc biệt) và không bao gồm khoảng trắng"
    );
    return validatedPassword;
  };
  var date = function () {
    var validDate = /^[0-9]+\/[0-9]+\/[0-9]+$/;
    var dateString = employee.date;
    var parts = dateString.split("/");
    var day = parts[1] * 1;
    var month = parts[0] * 1;
    var year = parts[2] * 1;
    var validatedDate = function () {
      var noticeEl = document.getElementById("tbNgay");
      var validType = validate(
        employee.date,
        validDate,
        "tbNgay",
        "Vui lòng nhập ngày bắt đầu làm việc!",
        "Ngày bắt đầu làm việc có dạng: mm/dd/yyyy"
      );
      if (validType) {
        if (year > 1900) {
          if (year % 4 == 0) {
            var monthDayLeapYearValid = validate_month_day_value(
              29,
              month,
              day,
              noticeEl
            );
            return monthDayLeapYearValid;
          }
          var monthDayNotLeapYearValid = validate_month_day_value(
            28,
            month,
            day,
            noticeEl
          );
          return monthDayNotLeapYearValid;
        }
        noticeEl.style.display = "inline-block";
        noticeEl.innerHTML = "Năm bắt đầu làm việc phải từ sau năm 1900";
        return false;
      }
      return false;
    };
    return validatedDate();
  };
  var basicSalary = function () {
    var validBasicSalary = /^[0-9]+$/;
    var basicSalaryStr = document.getElementById("luongCB").value;
    var noticeEl = document.getElementById("tbLuongCB");
    var validatedBasicSalary = function () {
      var basicSalaryNumberValid = validate(
        basicSalaryStr,
        validBasicSalary,
        "tbLuongCB",
        "Vui lòng nhập lương cơ bản!",
        "Vui lòng nhập chữ số"
      );
      if (basicSalaryNumberValid) {
        if (
          employee.basicSalary >= 1000000 &&
          employee.basicSalary <= 20000000
        ) {
          noticeEl.style.display = "none";
          noticeEl.innerHTML = "";
          return true;
        }
        noticeEl.style.display = "inline-block";
        noticeEl.innerHTML =
          "Lương cơ bản tối thiểu là 1 triệu và tối đa là 20 triệu";
        return false;
      }
    };
    return validatedBasicSalary();
  };
  var position = function () {
    var validPosition = /\bSếp\b|\bTrưởng phòng\b|\bNhân viên\b/;
    var validatedPosition = validate(
      employee.position,
      validPosition,
      "tbChucVu",
      "Vui lòng chọn chức vụ!",
      "Vui lòng chọn chức vụ!"
    );
    return validatedPosition;
  };
  var workingTime = function () {
    var workingTimeStr = document.getElementById("gioLam").value;
    var noticeEl = document.getElementById("tbGiolam");
    var validWorkingTime = /^\d+$/;
    var validType = validate(
      workingTimeStr,
      validWorkingTime,
      "tbGiolam",
      "Vui lòng nhập số giờ làm!",
      "Vui lòng nhập chữ số"
    );
    var validatedWorkingTime = function () {
      if (validType) {
        if (employee.workingTime >= 80 && employee.workingTime <= 200) {
          noticeEl.style.display = "none";
          noticeEl.innerHTML = "";
          return true;
        }
        noticeEl.style.display = "inline-block";
        noticeEl.innerHTML =
          "Số giờ làm tối thiểu trong tháng là 80 và tối đa là 200";
        return false;
      }
    };
    return validatedWorkingTime();
  };
  var valid =
    name() &
    email() &
    password() &
    date() &
    basicSalary() &
    position() &
    workingTime();
  return valid;
}
