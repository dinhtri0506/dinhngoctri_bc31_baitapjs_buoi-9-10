function render_classification_list(classificationList, noticeArray) {
  var contentHTML = [];
  var innerEL = document.getElementById("ulPhanTrang");
  if (noticeArray.length == 0) {
    contentHTML.push(
      `<li>Vui lòng nhập đúng xếp loại nhân viên cần tìm (xuất sắc, giỏi, khá hoặc trung bình)</li>`
    );
    return (innerEL.innerHTML = contentHTML.join(""));
  }
  if (classificationList.length == 0) {
    contentHTML.push(
      `<li>Không có nhân viên loại ${noticeArray.join(" hay ")}</li>`
    );
    return (innerEL.innerHTML = contentHTML.join(""));
  }
  contentHTML.push(
    `<li class="font-weight-bold">${classificationList[0].classification()}</li>`
  );
  var k = 0;
  for (i = 0; i < noticeArray.length; i++) {
    var item = removeAscent(noticeArray[i]);
    var regex = new RegExp(item, "i");
    for (j = k; k < classificationList.length; k++) {
      var employee = classificationList[k];
      var checkingValue = removeAscent(employee.classification());
      if (regex.test(checkingValue)) {
        var contentRender = `<li>${employee.fullName} (${employee.workingTime} giờ làm)</li>`;
        contentHTML.push(contentRender);
      } else {
        contentHTML.push(
          `<li class="font-weight-bold">${employee.classification()}</li>`
        );
        break;
      }
    }
  }
  for (l = 0; l < contentHTML.length; l++) {
    var a = l + 1;

    if (contentHTML[l] == contentHTML[a]) {
      contentHTML.splice(l, 1);
      --l;
    }
  }
  return (innerEL.innerHTML = contentHTML.join(""));
}
