function get_info_from_localStorge_and_render_it() {
  var dataJSON = localStorage.getItem("Employee_List");
  if (dataJSON !== null) {
    var empolyeeArray = JSON.parse(dataJSON);
    for (i = 0; i < empolyeeArray.length; i++) {
      var item = empolyeeArray[i];
      var account = item.account;
      var fullName = item.fullName;
      var email = item.email;
      var password = item.password;
      var date = item.date;
      var basicSalary = item.basicSalary;
      var position = item.position;
      var workingTime = item.workingTime;
      var employee = new Employee(
        account,
        fullName,
        email,
        password,
        date,
        basicSalary,
        position,
        workingTime
      );
      employeeList.push(employee);
    }
    render_employee_information(employeeList);
  }
}
get_info_from_localStorge_and_render_it();
