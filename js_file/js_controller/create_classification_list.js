function create_classification_list() {
  var classificationList = [];
  var noticeArray = [];
  var searchValue = document.getElementById("searchName").value;
  var searchValueAscentRemoved = removeAscent(searchValue);
  var listForSearching = [];
  // Tạo một mảng mới để khi splice không bị ảnh hưởng mảng gốc
  for (i = 0; i < employeeList.length; i++) {
    listForSearching.push(employeeList[i]);
  }
  function push_item_for_classification_list(valid, type) {
    if (valid.test(searchValueAscentRemoved)) {
      noticeArray.push(type);
      for (i = 0; i < listForSearching.length; i++) {
        var employee = listForSearching[i];
        var employeeClassification = listForSearching[i].classification();
        if (valid.test(removeAscent(employeeClassification))) {
          classificationList.push(employee);
          // splice để lần gọi hàm kể tiếp, không phải kiểm tra lại những item đã push
          listForSearching.splice(i, 1);
          // sau khi splice thì item trong mảng bớt đi 1, vì vậy cũng ở vị trí i nhưng item lúc này là i+1 (so với trước khi splice). Vì vậy phải trừ 1 trước khi vòng lặp bắt đầu cộng 1, để tránh bỏ sót item
          --i;
        }
      }
    }
  }
  var excellentEmployee = /\bxuat sac\b/i;
  push_item_for_classification_list(excellentEmployee, "xuất sắc");
  var goodEmployee = /\bgioi\b/i;
  push_item_for_classification_list(goodEmployee, "giỏi");
  var fairlyEmployee = /\bkha\b/i;
  push_item_for_classification_list(fairlyEmployee, "khá");
  var averageEmployee = /\btrung\b|\bbinh\b/i;
  push_item_for_classification_list(averageEmployee, "trung bình");

  return [classificationList, noticeArray];
}
