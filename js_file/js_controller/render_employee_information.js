function render_employee_information() {
  var contentHTML = "";
  for (i = 0; i < employeeList.length; i++) {
    var employee = employeeList[i];
    var contentRender = `<tr>
    <td>${employee.account}</td>
    <td>${employee.fullName}</td>
    <td>${employee.email}</td>
    <td>${employee.date}</td>
    <td>${employee.position}</td>
    <td>${employee.calculate_total_salary()}</td>
    <td>${employee.classification()}</td>
    <td>
    <button class="delete-btn btn btn-danger" onclick="delete_employee_info(${
      employee.account
    })">
    <i class="fa fa-trash"></i>
    </button>
    <button class="change-info-btn btn btn-warning" 
    data-toggle="modal" 
    data-target="#myModal"
    onclick="change_employee_info(${employee.account})">
    <i class="fa fa-reply"></i>
    </button>
    </td>
</tr>`;
    contentHTML += contentRender;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
