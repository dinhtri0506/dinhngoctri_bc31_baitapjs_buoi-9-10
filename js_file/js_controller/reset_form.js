function reset_form() {
  var d = new Date();
  var today = d.getDate();
  var month = d.getMonth() + 1;
  function date_output(value) {
    if (("" + value).length < 2) {
      return "0" + value;
    } else {
      return value;
    }
  }
  var output =
    date_output(month) + "/" + date_output(today) + "/" + d.getFullYear();
  accountEl.disabled = false;
  document.getElementById("tknv").value = "";
  document.getElementById("name").value = "";
  document.getElementById("email").value = "";
  document.getElementById("password").value = "";
  document.getElementById("datepicker").value = output;
  document.getElementById("luongCB").value = "";
  document.getElementById("chucvu").value = "Chọn chức vụ";
  document.getElementById("gioLam").value = "";

  // var accountNoticeEl = document.getElementById("tbTKNV");
  // accountNoticeEl.value = "";
  // accountNoticeEl.style.display = "none";
  // var fullNameNoticeEl = document.getElementById("tbTen");
  // fullNameNoticeEl.value = "";
  // fullNameNoticeEl.style.display = "none";
  // var emailNoticeEl = document.getElementById("tbEmail");
  // emailNoticeEl.value = "";
  // emailNoticeEl.style.display = "none";

  // fix code 15/06/2022
  var noticeEl = document.querySelectorAll(".sp-thongbao");
  console.log("noticeEl: ", noticeEl.length);
  for (i = 0; i < noticeEl.length; i++) {
    noticeEl[i].style.display = "none";
    noticeEl[i].innerHTML = "";
  }
}
