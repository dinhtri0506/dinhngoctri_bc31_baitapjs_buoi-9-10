function pull_employee_info_back_to_input_element(employeeList, index) {
  var employee = employeeList[index];
  document.getElementById("tknv").value = employee.account;
  document.getElementById("name").value = employee.fullName;
  document.getElementById("email").value = employee.email;
  document.getElementById("password").value = employee.password;
  document.getElementById("datepicker").value = employee.date;
  document.getElementById("luongCB").value = employee.basicSalary;
  document.getElementById("chucvu").value = employee.position;
  document.getElementById("gioLam").value = employee.workingTime;
}
